#include <iostream>
#include <string>
#include <fstream>
#include <cassert>
#include <memory>
#include <boost/algorithm/string.hpp>

struct Buyer
{

	std::string who = "";
	double price = 0.0;
};

struct Result
{
	~Result()
	{
		delete winner;
		delete second;
	}
	double objectPrice = 0.0;
	Buyer *winner = nullptr;
	Buyer *second = nullptr;
};

void parseBids(const std::string &line, std::string &currentBuyer, std::set<double> &bidsSet)
{
	//case of bids
	std::vector<std::string> bids;
	boost::split(bids, line, [](char c) { return c == ' '; });

	currentBuyer = bids.at(0);
	for (uint32_t i = 1; i < bids.size(); i++)
	{
		bidsSet.insert(std::stod(bids.at(i)));
	}
}

void whoWin(const std::string &filename, Result &result)
{
	std::string line;
	std::ifstream ifile(filename);

	if (ifile.is_open())
	{
		bool firstline = true;
		while (std::getline(ifile, line))
		{
			// case object price
			if (firstline)
			{
				result.objectPrice = std::stod(line);
				firstline = false;
				continue;
			}
			// case of bid  :  BuyerID  bid1 bid2 ....
			std::string currentBuyer;
			std::set<double> bidsSet;
			parseBids(line, currentBuyer, bidsSet);
			if (bidsSet.empty())
				continue;

			double maxBid = *bidsSet.rbegin();
			if (!result.winner)
			{
				result.winner = new Buyer();
				result.winner->who = currentBuyer;
				result.winner->price = maxBid;
			}
			else if (maxBid > result.winner->price)
			{
				result.second = result.winner;
				result.winner = new Buyer();
				result.winner->who = currentBuyer;
				result.winner->price = maxBid;
			}
			else if (maxBid == result.winner->price)
			{
				delete result.second;
				result.second = new Buyer();
				result.second->who = currentBuyer;
				result.second->price = maxBid;
			}
		}
		ifile.close();
	}

	else
	{
		std::cout << "Unable to open file";
	}
}

// test function
int tests(const std::string &filename, const std::string &winner, double bidPrice, double price)
{
	Result result;
	try
	{
		whoWin(filename, result);
	}
	catch (...)
	{
		std::cerr << "Error parsing";
		return -1;
	}

	// no buyers
	if (!result.winner)
	{
		std::cout << "No buyers Or erreur parsing\n";
		return 2;
	}

	// no buyer with bid greater than object price
	if (result.winner->price < result.objectPrice)
	{
		std::cout << " All bids are less than object price \n";
		return 3;
	}

	// checke second price greater than object price
	if (result.second->price < result.objectPrice)
	{
		result.second->price = result.objectPrice;
		result.second->who = "The object Price";
	}

	std::cout << "Winner is " << result.winner->who << " with bid " << result.winner->price << " \n";
	std::cout << "He will pay " << result.second->price << " ; the  bid of " << result.second->who << " \n";

	assert(result.winner->who == winner);
	assert(result.winner->price == bidPrice);
	assert(result.second->price == price);
	return 0;
}

int main()
{
	tests("./testFiles/teads", "E", 140, 130);
	tests("./testFiles/same-bids-winner", "E", 140, 140);
	assert(tests("./testFiles/no-bids", "", 0.0, 0.0) == 2);
	tests("./testFiles/bids-less-than-reserve", "E", 141, 100);
	assert(tests("./testFiles/bids-less-than-reserve-all", "", 0.0, 0.0) == 3);
	// tests("../bigtest", "Buyer1762", 299357, 299357);
	return 0;
}
